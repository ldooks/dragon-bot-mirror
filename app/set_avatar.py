import requests

import core_utils
import get_from_reddit

def change_bots_avatar():
    image = None
    while not image:
        image = get_from_reddit.get_image(boards='smuganimegirls')
        extension = image.split('.')[-1]
        local_smug = "/tmp/smug.{}".format(extension)

        if extension.lower() in ['png', 'jpg']:
            # save an image locally
            return open(core_utils.download_image(image, local_smug), 'rb').read()
        else:
            image = None
