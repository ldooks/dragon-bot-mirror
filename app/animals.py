import random
import requests


def get_dog():
    return random.choice([random_sheeb, random_dog])()


def random_sheeb():
    return requests.get(
      'http://shibe.online/api/shibes?count=1&urls=true'
    ).text.split('"')[1]


def random_dog():
    dog_url = None
    while not dog_url:
        dog_url = requests.get('https://random.dog/woof').text
        if 'mp4' in dog_url[-3:]:
            dog_url = None
    return  "https://random.dog/{}".format(dog_url)


def get_birb():
    return "https://random.birb.pw/img/{}".format(
        requests.get(
            'https://random.birb.pw/tweet'
        ).text)