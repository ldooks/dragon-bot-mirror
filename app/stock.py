import requests

def parse_share(msg):
    if len(msg.split()) > 1:
        try:
            res = ''
            for s in msg.split()[1:]:
                res = '{}\n\n{}'.format(res, get_stock(s))
        except:
            res = '```Please input valid shares: !stock [share_name]```'
        return res
    return '```Please input at least one valid share: !stock [share_name]```'

def get_stock(share_name):

    request = requests.get("https://api.iextrading.com/1.0/stock/{}/quote".format(share_name)).json()

    return '```Company Name: {}\nSymbol: {}\nSector: {}\n\nOpen: ${}\nCurrently: ${}\nChange: ${}\nChange percent {}%```'.format(
        request['companyName'],
        request['symbol'],
        request['sector'],
        request['open'],
        request['latestPrice'],
        request['change'],
        request['changePercent']
    )
