import requests
import os

import help_methods
import role_check


def create_issue(title, description):

    post_args = {
        'title': title,
        'description': description
    }

    headers = {
        'PRIVATE-TOKEN': os.getenv('gitlab_token')
    }

    r = requests.post(
        'http://git.luker.gq/api/v4/projects/2/issues',
        data=post_args,
        headers=headers
    )

    return(r.json()['web_url'])


def parse_message(message):
    if not role_check.is_admin(message.author.roles):
        return 'You dont have permission to do that'

    if len(message.content.split()) == 1:
        return help_methods.get_help_message(method='issue')

    try:
        message = ' '.join(message.content.split()[1:])
        title, description = message.split(';')
    except Exception:
        return help_methods.get_help_message(method='issue')

    try:
        return create_issue(title=title, description=description)
    except Exception:
        return help_methods.get_help_message(method='issue')
