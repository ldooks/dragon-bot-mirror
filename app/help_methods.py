import discord
def get_help_message(method):
    """
    get_help_message(method)

    Returns the information and usage of each of dragon bot's funtions.
    When adding a new feature to dragon bot, please add some information about
    it here
    """

    supported_methods = {
        '8ball': [
            'Ask dragon bot a question and it will give you an 8ball response',
            '\nUsage: !8ball Will I win the lottery tomorrow?'
            ],
        'avatar': [
            'Returns the avatar for the mentioned user',
            '\nUsage: !avatar @somebody'
            ],
        'birb': [
            'Sends an image of a birb',
            '\nUsage: !birb'
            ],
        'clap': [
            'Returns the shittiest meme created by sassy girls on twitter this century',
            'Usage: !clap some text to be meme\'mt'
            ],
        'cleanup': [
            'Admin use only. Deletes dragon bot\'s messages from the channel',
            'Usage: !cleanup'
            ],
        'decide': [
            'Dragon-bot will help make the tough decisions for you',
            ' If there is only one option, it will give you a yes or no',
            '\nUsage: !decide cake or pie\n!decide should I do my homework'
            ],
        'define': [
            'Returns a definiton of a word from urban dictionary',
            '\nUsage: !define loli'
            ],
        'minecraft': [
            'Allows you to interact with the minecraft server.',
            'You can do things like !minecraft logs, or !minecraft map',
            ' or !minecraft status to get information about the server'
            ],
        'dog': [
            'Returns the URL to a G O O D B O Y E or G I R L',
            '\nUsage: !dog'
            ],
        'emoji': [
            'Uploads the passed in URL to the server as an emoji.',
            '\nDiscord does not support GIFS. It will throw and error if you try.'
            '\nCurrently only admins can do this',
            '\nUsage: !emoji http://pictures.com/some_image.png my_new_emoji\n',
            'or `!emoji del some_emoji` to delete'
            ],
        'excuse': [
            'Generates a random excuse you can give your boss',
            '\nUsage: !excuse'
            ],
        'greentext': [
            'Grabs a greentext story from redditchat so you can laugh at the misfortune of others',
            '\nUsage: !greentext'
            ],
        'help': [
            'Shows you this message'
            ],
        'invite': [
            'Generates a one time use invite to the current channel.',
            'Be quick! It expires in 12 hours'
            ],
        'issue': [
            'Creates an issue on gitlab with the passed in parameters\n',
            'Usage: !issue <issue title>; <issue description>',
            ],
        'lewd': [
            'Returns a URL for a lewd image.',
            'Can only be used in NSFW channels.',
            '\nUsage: !lewd'
            ],
        'purge': [
            'By default, will delete your last 20 messages. You can override this',
            ' with your own number. \nUsage: !purge or !purge 15'
            ],
        'roles': [
            'Dragon bot will PM you a message with all the roles you have on the server'
            ],
        'wallpaper': [
            'Returns the URL for a 4k wallpaper. You can enter',
            'a search term as well. Supports multiple search terms as well',
            '\nUsage: !wallpaper flowers or !wallpaper mountain flowers sky '
            ],
        'message': [
            'You can ask me a question directly and I will do my best to answer it.',
            '\nUsage: @dragon-bot what is the capital of France?'
            ],
        'meme': [
            'Generates a meme on the fly!',
            '\nExamples of all templates can be seen here https://memegen.link/examples'
            '\n\nUsage: !meme doge top text; bottom text'
            ],
        'homepage': [
            'This function now outputs the SWEET-ASS picture of the day.',
            ' Note this picture only changes once a day.',
            '\nUsage: !homepage'
            ],
        'weather': [
            'Returns the weather for the location you entered.',
            '\nUsage: !weather Berkeley'
            ],
        'pout': [
            'Returns the URL for an anime girl pouting you filthy weeb\n',
            'Usage: !pout'
            ],
        'quake': [
            'Returns player stats for a quake champions account\n'
            'Usage: !quake <player name>'
            ],
        'roll': [
            'Rolls N number of Y sided die\n'
            'Usage: !roll 3 20'
            ],
        'smug': [
            'Returns the URL for smug anime girl'
            ],
        'source': [
            "Links you to the git repo with dragon-bot's source code"
            ],
        'stock': [
            'Returns basic stock information for the stock you entered.',
            '\nUsage: !stock AAPL TSLA'
            ],
        'tts': [
            'Uploads a file with the specified text as an MP3.\nThis is advanced shitposting',
            '\nUsage: !tts who watches the watchmen?',
            '\nTo list all languages, you can type `!tts langs`'
            ],
    }

    return "```css\n{}: {}\n```".format(method, ' '.join(supported_methods[method]))

def get_help_embed(client):
    categories = {
        'fun': ['clap', 'birb', 'dog', 'excuse', 'greentext', 'lewd', 'message', 'meme', 'homepage', 'pout', 'roll', 'smug', 'quake'],
        'util': ['8ball', 'decide', 'wallpaper', 'weather', 'stock', 'tts', 'issue'],
        'users': ['help', 'invite', 'purge', 'roles', 'source', 'minecraft'],
        'admin': ['emoji', 'cleanup']
    }

    description = "\nBelow you can see all the commands I know.\n\n**Have a nice day!**"
    embed = discord.Embed(description=description, color=0x428bca, type="rich")
    embed.set_author(name="Hello! I'm {}".format(client.user.name), icon_url=client.user.default_avatar_url)

    for category in categories:
        command_list = []
        for command in categories[category]:
            command_list.append("`{}`".format(command))
        embed.add_field(name=str(category).upper(), value=', '.join(command_list), inline=False)

    description2 = "**Use `!help <Command>` for more information about a command.** \n\n" + \
                    "**Examples:** \n `!help dog` for detailed help for the dog command \n\n" + \
                    "**Useful links:** [My source code](http://git.luker.gq/ldooks/dragon-bot), [Server status](http://luker.gq/status), [Donate](http://luker.gq/donate)"

    embed.add_field(name="\u200b", value=description2, inline=False)
    return embed

def parse_message(message):
    method = message.split()[1]
    try:
        explanation = get_help_message(method)
    except KeyError:
        return "This command could not be found."
    return explanation
