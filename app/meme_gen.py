import os
import requests

import help_methods

supported_templates = sorted(list(map(lambda x: x.split('/')[-1], requests.get('https://memegen.link/api/templates/').json().values())))

def parse_message(message):
    if len(message.split()) <= 2:
        return get_meme_help()

    escaped_chars = {'_' : '__', ' ' : '_', '-' : '--', '?' : '~q', '%' : '~p', '#' : '~h', '/' : '~s', '\'\'' : '\"'}

    # Unwrap message from discord
    template = message.split()[1]
    text = message.replace('!meme {}'.format(template), '').lstrip().split(',')
    upper = text[0].split(';')[0].lstrip()
    lower = text[0].split(';')[1].lstrip()

    # Escape special characters in upper/lower text
    for char, escaped in escaped_chars.items():
        upper = upper.replace(char, escaped)
        lower = lower.replace(char, escaped)

    usesTemplate = True
    if template.startswith('http'):
        usesTemplate = False
    elif template not in supported_templates:
        return 'Template not supported!'

    return get_meme_url(template, upper, lower, usesTemplate)

def get_meme_url(template, upper, lower, usesTemplate):
    #TODO: Implement format as a parameter?
    base_URL = 'https://memegen.link'
    custom_URL = '{}/custom'.format(base_URL)
    default_format = 'jpg'

    # Generate meme url
    meme = '{}/{}/{}.{}?alt={}'.format(custom_URL, upper, lower, default_format, template)
    if usesTemplate:
        meme = '{}/{}/{}/{}.{}'.format(base_URL, template, upper, lower, default_format)
    return meme

def get_meme_help():
  return "{}\nYou must supply a valid template for the meme. Templates are one of the following: \n```{}```".format(
      help_methods.get_help_message('meme'),
      ', '.join(supported_templates)
    )
