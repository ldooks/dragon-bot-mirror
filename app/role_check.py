"""
role_check

This module's purpose is to handle permissions for interacting with the bot.
The end goal is to be able to lock certain actions down to certain roles
"""

def check_permissions(user, roles_to_check):
    """
    check_permissions(user)

    Parses the list object that is passed in and returns true or false if
    the user has the correct roles
    """
    users_roles = []
    for role in user:
        users_roles.append(role.name)

    # Returns true if the user contains a role in the roles_to_check list
    return not set(users_roles).isdisjoint(roles_to_check)


def is_admin(user):
    """
    is_admin(user)

    Returns true if the user contains the ADMIN role
    """
    return check_permissions(user, ['ADMIN'])


def is_mod(user):
    """
    is_admin(user)

    Returns true if the user contains the Moderator role
    """
    return check_permissions(user, ['Moderator'])


def docker_permissions(user):
    """
    docker_permissions(user)

    Checks that the user has permissions to have dragon-bot run docker commands
    """
    return is_mod(user) or is_admin(user)


def cleanup_permissions(user):
    """
    cleanup_permissions(user)

    Who has rights to make dragon-bot purge its messages
    """
    return is_admin(user) or is_mod(user)
