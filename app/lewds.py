import random
import requests

import get_from_reddit


def get_from_danbooru(boards, nsfw=True):
    """
    get_from_danbooru(boards)

    returns a URL to an image on danbooru
    """

    request = requests.get(
        'https://danbooru.donmai.us/posts/random.json?tags=rating%3Aexplicit'
    ).json()

    # List of tags we dont want images from
    undesired_tags = [
        'bestiality',
        'pee',
        'futa',
        'futanari',
        'yaoi'
    ]
    # If any undesired tags show up in the request, try again
    if not set(request['tag_string'].split()).isdisjoint(undesired_tags):
        return get_from_danbooru(boards)

    # Return the large firl url if available
    if 'large_file_url' in request.keys():
        if request['large_file_url'].startswith('/data/'):
            return "https://danbooru.donmai.us{}".format(request['large_file_url'])
        return request['large_file_url']

    if 'file_url' in request.keys():
        if request['file_url'].startswith('/data/'):
            return "https://danbooru.donmai.us{}".format(request['file_url'])
        return request['file_url']

    return get_from_danbooru(boards)


def get_lewd():
    """
    get_lewd()

    Chooses between getting from reddit or danbooru
    If reddit is chosen, picks a random subreddit from the list of boards and
    grabs a random image using the get_from_reddit() method
    """
    # List of subreddits that have the lewds we seek
    boards = [
        '2Booty',
        'ahegao',
        'bakunyuu_hentai',
        'BigAnimeTiddies',
        'hentaibreeding',
        'hentaiclevage',
        'oppailove',
        'ecchi',
        'hentai',
        'rule34',
        'rule34lol',
        'WesternHentai',
        'doujinshi',
        'hentai_gif',
        'muchihentai',
        'Paizuri',
        'chiisaihentai',
        'dekaihentai',
        'WaifusGonewild',
        'thighdeology'
    ]

    # This is really bad practice but pass boards to get_from_reddit AND danbooru so it doesnt error
    return random.choice([get_from_reddit.get_image, get_from_danbooru])(boards=boards, nsfw=True)
