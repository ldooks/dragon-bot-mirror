from gtts import gTTS
from gtts import lang

def text_to_speech(input):
    """
    text_to_speech(input)
    receives the message from discord and returns an MP3 of it.
    If you pass in a language after a semicolon, it will return it read in
    that language. eg: what are you doing; ja, will return a file read by a japanese
    robot
    """
    file_path = '/tmp/memes.mp3'
    message = ' '.join(input.split()[1:])

    language = 'en'
    # check for another language denoted at the end of the message
    if ';' in message:
        print(message.split(';'))
        language = message.split(';')[1].strip()
        message = message.split(';')[0]

    gTTS(
        text=message,
        lang=language,
        slow=False
    ).save(file_path)

    return file_path

def get_all_langs():
    """
    get_all_langs()
    
    returns a dictionary with all supported languages
    """
    blob = ""
    for key, value in lang.tts_langs().items():
        blob += "{}: {}\n".format(key, value)

    return "```css\n{}```".format(blob)
