import random
import sys

import help_methods

def check_8ball(question):
    """
    check_8ball(question)

    Takes in a question and returns a random answer from an 8ball.
    To add more responses, simply add it to one of the lists below
    """
    if len(question.split()) > 1:
        question = question.split()[1:]

        positive_responses = [
          'As I see it, yes',
          'It is certain',
          'It is decidedly so',
          'Most likely',
          'Oh for sure brother',
          'Outlook good',
          'Signs point to yes',
          'Without a doubt',
          'Yes, definitely',
          'Yes',
          'You can count on it',
        ]
        negative_responses = [
          'Dont count on it',
          'My sources say no',
          'My thoughts are.....no',
          'Outlook not so good',
          'Very doubtful',
        ]
        noncommittal_responses = [
          'Ask again later',
          'Better not tell you now',
          'Can\'t answer that right now',
          'Concentrate and ask again',
          'Reply hazy try again',
        ]

        return random.choice(
            positive_responses + 
            negative_responses +
            noncommittal_responses
        )

    return help_methods.get_help_message('8ball')