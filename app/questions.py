import wikipedia
import wolframalpha

import help_methods

def answer_question(message):
    """
    answer_question(question)

    Submits a request to the wolfram API and returns the response
    If no answer is found, tries wikipedia. If that fails, apologizes
    """

    if len(message.split()) > 1:
        client = wolframalpha.Client('2LU2Y7-YJQTA7TL8E')
        question = ' '.join(message.split()[1:])
        try:
            res = client.query(question)
            return next(res.results).text
        except Exception:
            try:
                return wikipedia.summary(question, sentences=5)
            except Exception:
                return "Sorry, I\'m unable to answer that"

    return help_methods.get_help_message('message')
