import requests
import help_methods

def get_definition(content):
    """
    get_definition(content)

    grabs a definition from urban dictionary
    """
    if len(content.split()) > 1:
        word = content.split()[1:]
        try:
            definition = requests.get(
                "https://api.urbandictionary.com/v0/define?term={}".format('%20'.join(word))
            ).json()['list'][0]['definition']

        except IndexError:
            try:
                # Try another dictionary
                definition = requests.get(
                    "http://api.pearson.com/v2/dictionaries/ldoce5/entries?headword={}".format('%20'.join(word))
                ).json()['results'][0]['senses'][0]['definition'][0]
            except IndexError:
                definition = 'No definition found'

        return "`{}`\n```{}```".format(' '.join(word), definition)

    return help_methods.get_help_message('define')
