import requests

def download_image(url, path=None):

    request = requests.get(url)
    suffix_list = ['jpeg', 'jpg', 'png', 'tif', 'svg',]
    extension = request.headers['content-type'].split('/')[1]

    if not path:
        path = "/tmp/image.{}".format(extension)

    if extension in suffix_list:
        open(path, 'wb').write(requests.get(url).content)
        return path
    return 'Invalid image format'
