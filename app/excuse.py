from bs4 import BeautifulSoup
import requests

def get_excuse():
  url = requests.get('http://www.devexcuses.com')
  soup = BeautifulSoup(url.content, features="html.parser")

  return "```{}```".format(str(soup.find("p", {"class": "excuse"}).contents[0]).split(">")[1].split("</a")[0])