from random import randint

def roll_logic(sides):
  return randint(1, sides)

def roll(n, sides):
  results = tuple(roll_logic(int(sides)) for _ in range(int(n)))
  message = "%s\nRolling %s %s sided die\n\n%s" % (':game_die:' * int(n), n, sides, results)
  return message

def parse_message(message):
  return roll(message.split()[1], message.split()[2])