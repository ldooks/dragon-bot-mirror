import os
import help_methods
import discord
import core_utils

async def create_emoji(client, message):
  emoji_staging = '/tmp/emoji'

  try:
    command, url, emoji_name = message.content.split()
  except Exception:
    await client.send_message(
      message.channel,
      help_methods.get_help_message('emoji')
    )
    return
  try:
    # Attempt to upload the emoji
    await client.delete_message(message)
    await client.send_message(
      message.channel,
      "emoji successfully uploaded! Heres how it looks in a sentence {}\nUse it with `:{}:`".format(
          await client.create_custom_emoji(
            server=message.server,
            name=emoji_name,
            image=open(core_utils.download_image(url, emoji_staging), "rb").read()
          ), emoji_name
      )
    )

  except Exception:
    await client.send_message(
      message.channel,
      "I wasnt able to upload that image as an emoji. Sorry"
    )
    os.remove(emoji_staging)
    return

async def delete_emoji(client, message):
  name = message.content.split()[2]
  emotes = [x for x in client.get_all_emojis() if x.name == name]
  if not len(emotes):
    return await client.send_message(
      message.channel,
      "No emotes with that name could be found on this server."
    )
  for emote in emotes:
    await client.delete_custom_emoji(emote)
    return await client.send_message(
      message.channel,
      "Successfully deleted"
    )

async def parse_message(client, message):
  if message.content.split()[1] == 'del':
    return await delete_emoji(client, message)

  await create_emoji(client, message)