from bs4 import BeautifulSoup
from urllib.parse import urlparse
import requests
import urllib

def get_wall(message):
    """
    get_wall(message)

    Grabs a random wallpaper from unsplash.com's random function
    If any tags are passed in as the message, it will search for images
    matching those tags
    """
    unsplash_url = "https://source.unsplash.com/3840x2160/?"

    if len(message.split()) > 2:
        search_terms = message.split()[1:]

        # Turn search_terms into strings separated by commas
        joined_terms = ','.join(search_terms)

        # Add those comma separated strings onto the end of the URL variable
        url = unsplash_url + joined_terms

    elif len(message.split()) > 1:
        term = message.split()[1]
        url = unsplash_url + term

    else:
        url = unsplash_url

    response = requests.get(url).url

    if 'photo-1446704477871-62a4972035cd' in response:
        return "Could not find an image for those tags."
    else:
        return response

def fcking_homepage():
    """
    fcking_homepage()

    Retrives the picture of the day from fuckinghomepage.com
    """
    url = requests.get("http://fuckinghomepage.com")
    soup = BeautifulSoup(url.content)

    for pic in soup.find_all("p"):
        if 'SWEET-ASS PICTURE' in ''.join(pic.findAll(text=True)):
            link = pic.find_next_sibling('p')
            if "http://" or "https://" in link.get('href', ''):
                link = link.find('small').find_next('a', href=True)['href']
                return urllib.parse.unquote(link.split('=')[1].split('&')[0])
