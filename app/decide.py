import random
import help_methods

def decide(message):
    choices = message.replace('!decide', '' ).lstrip().split(' or ')
    if '' in choices:
        return help_methods.get_help_message('decide')
    elif len(choices) > 1:
        return random.choice(choices)
    else:
        return random.choice(["yes", "no"])
