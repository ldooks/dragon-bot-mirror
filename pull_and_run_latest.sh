# Remove the running container so we cna re-use the container name 'dragon-bot'
printf "\n[-] Deleting old dragon-bot container from system\n"
docker rm -f dragon-bot
printf "[+] Done\n"

# Run that shit and mount the docker socket so it can talk to the sky-factory container
printf "\n[-] Pulling latest build of container\n"
docker pull ldooks/dragon-bot:latest
docker run -d --name dragon-bot --env-file /home/ldooks/dragon_bot_env -v /var/run/docker.sock:/var/run/docker.sock --restart always ldooks/dragon-bot:latest
printf "[+] Done\n"
