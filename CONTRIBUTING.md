### Contribution guidelines ###
* Make your changes
* Test the bot by running `test-dragon-bot.sh` and interacting with the bot in the `#dragon-bot-test` channel
  * This will lint your python as well
* Create your pull request
	* bonus points if you include screenshots, terminal output, or even videos of the features on the PR