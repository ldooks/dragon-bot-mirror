[![pipeline status](https://git.luker.gq/ldooks/dragon-bot/badges/master/pipeline.svg)](https://git.luker.gq/ldooks/dragon-bot/commits/master)
# README #

A discord bot written in python.

This project was used mainly to teach members of the discord channel about software development, docker, CI/CD, and for me to learn about project management

### What does this bot do? ###
* This is a chat bot of sorts that connects to our [discord](https://discordapp.com/) channel and interacts with us via chat commands
* This bot has also been an ongoing learning experience for several members of the discord channel. With this bot, I have been able to teach the repo contributors:
	* Docker
	* Git
	* Python
	* CI/CD
	* Collaborative coding practices
	* Testing methodologies
	* Code Accountability

### What is this repository for? ###

* Here we track issues and collaborate on the bot

## How do I get set up? ###

### Installing docker
* linux
	* as root (`sudo -s`)
	* `curl -Ls get.docker.com | bash`
	* Give yourself docker permissions `sudo usermod -aG docker <your username>`
* OS X
	* `curl -L get.macapps.link/en/docker | bash`
	* open /Applicaitons/Docker.app
	* Complete the setup steps
* Windows
  * See [This](https://medium.com/@sebagomez/installing-the-docker-client-on-ubuntus-windows-subsystem-for-linux-612b392a44c4) guide


### Who do I talk to? ###

* Bad dragon in discord
