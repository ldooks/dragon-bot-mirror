FROM python:3.6.2-alpine3.6
ADD app/requirements.txt /requirements.txt
RUN apk update && \
	apk add --no-cache docker && \
	pip install -U pip && \
	pip install -r requirements.txt
ADD app /app
CMD python /app/dragon-bot.py
